package pl.pmajewski.xtm.recruitment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XtmRecruitmentApplication {

    public static void main(String[] args) {
        SpringApplication.run(XtmRecruitmentApplication.class, args);
    }

}
