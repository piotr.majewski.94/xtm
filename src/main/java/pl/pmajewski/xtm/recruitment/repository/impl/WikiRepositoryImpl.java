package pl.pmajewski.xtm.recruitment.repository.impl;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import pl.pmajewski.xtm.recruitment.dto.WikiResponse;
import pl.pmajewski.xtm.recruitment.repository.WikiRepository;

import static pl.pmajewski.xtm.recruitment.config.Constants.WIKI_BASE_SEARCH;

@Component
public class WikiRepositoryImpl implements WikiRepository {

    private RestTemplate webClient;

    public WikiRepositoryImpl(RestTemplate webClient) {
        this.webClient = webClient;
    }

    @Override
    public WikiResponse search(String keyword) {
        String url = WIKI_BASE_SEARCH + "?action=query&list=search&format=json&srsearch=%22{keyword}%22&srlimit=10";
        return webClient.getForObject(url, WikiResponse.class, keyword);
    }
}
