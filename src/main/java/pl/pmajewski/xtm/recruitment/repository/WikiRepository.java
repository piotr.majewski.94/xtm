package pl.pmajewski.xtm.recruitment.repository;

import pl.pmajewski.xtm.recruitment.dto.WikiResponse;

public interface WikiRepository {

    WikiResponse search(String keyword);
}
