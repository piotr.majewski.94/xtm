package pl.pmajewski.xtm.recruitment.dto;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;

@Data
public class WikiResponse {

    private String batchcomplete;
    @JsonAlias("continue")
    private WikiResponseSequence _continue;
    private WikiResponseQuery query;

}
