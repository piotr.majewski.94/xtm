package pl.pmajewski.xtm.recruitment.dto;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;

import java.util.List;

@Data
public class WikiResponseQuery {

    @JsonAlias("searchinfo")
    private WikiResponseSearchInfo searchInfo;
    private List<WikiResponseRecord> search;
}
