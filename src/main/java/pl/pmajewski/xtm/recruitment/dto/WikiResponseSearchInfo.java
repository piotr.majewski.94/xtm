package pl.pmajewski.xtm.recruitment.dto;

import lombok.Data;

@Data
public class WikiResponseSearchInfo {

    private Integer totalhits;
}
