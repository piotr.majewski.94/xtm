package pl.pmajewski.xtm.recruitment.dto;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;

@Data
public class WikiResponseSequence {

    private Integer sroffset;
    @JsonAlias("continue")
    private String _continue;
}
