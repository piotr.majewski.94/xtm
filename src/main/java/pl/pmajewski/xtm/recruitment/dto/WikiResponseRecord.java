package pl.pmajewski.xtm.recruitment.dto;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;

@Data
public class WikiResponseRecord {

    @JsonAlias("ns")
    private Long time;
    private String title;
    @JsonAlias("pageid")
    private Integer pageId;
    private Integer size;
    private Integer wordcount;
    private String snippet;
    private String timestamp;
}
