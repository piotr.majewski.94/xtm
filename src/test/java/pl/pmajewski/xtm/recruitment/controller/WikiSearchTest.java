package pl.pmajewski.xtm.recruitment.controller;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;
import pl.pmajewski.xtm.recruitment.config.Constants;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.anything;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

@SpringBootTest
class WikiSearchTest {

    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private WikiSearch wikiSearch;

    private MockRestServiceServer mockServer;

    @BeforeEach
    public void init() {
        mockServer = MockRestServiceServer.createServer(restTemplate);
    }

    @Test
    public void correctFootballClubHit() throws IOException {
        String responseJSON = IOUtils.toString(getClass().getClassLoader().getResourceAsStream("response.json"), StandardCharsets.UTF_8);
        mockServer.expect(ExpectedCount.manyTimes(), anything())
                .andRespond(withStatus(HttpStatus.OK)
                .body(responseJSON));

        String response = wikiSearch.search("Liverpool");
        assertEquals(Constants.WIKI_DEFINITION_BASE+ "Liverpool_F.C.", response);
    }
}